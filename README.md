# Formula Plex

This project provides a sample implementation of a fully automated system for downloading and watching Formula 1 races.
It consists of several applications working together:

* [Plex](https://www.plex.tv/): A media server which allows you to stream your content on various devices.
* [Plex Meta Manager](https://metamanager.wiki/en/latest/) (PMM): Populates Plex with specific metadata pertaining to your Plex media.
Instead of generic filenames parsed by Plex, it will standardize how your race sessions are named.
* [docker-transmission-openvpn](https://haugene.github.io/docker-transmission-openvpn/): Transmission torrent client with WebUI over an OpenVPN tunnel.
This will download races as they become available.
* [Reddit F1 Scanner](https://gitlab.com/lyle_/reddit-f1-scanner): Searches for Formula 1 torrents posted on [r/MotorsportsReplays](https://www.reddit.com/r/MotorsportsReplays/) and sends them to Transmission for downloading.
* [Plex F1 Renamer](https://gitlab.com/lyle_/plex-f1-renamer): A utility run upon completion of a Transmission torrent which copies (using [hardlinks](https://trash-guides.info/Hardlinks/Hardlinks-and-Instant-Moves/) to save disk space) a newly-completed Formula 1 download to a target directory with standardized naming for ingestion and proper display in a Plex library.

This is meant to be a complete working example starting from scratch.
If you have an existing Plex server, adapting it to your existing setup is left as an exercise to the reader.
If you already use Docker Compose (recommended), it should be fairly straightforward to add the additional containers required.

[[_TOC_]]

## Configuration

### .env file
Docker Compose needs to know your VPN settings and Plex Token to configure the system properly.

Copy the provided `example.env` file to `.env` and populate the following values:

| Variable | Description | Default |
| -------- | ----------- |---------|
| `OPENVPN_*` | These configure the Transmission container to use the VPN of your choice.<br/><br/>See the [docker-openvpn-transmission](https://haugene.github.io/docker-transmission-openvpn/config-options/) documentation for more details. | *none*  |
| `PLEX_TOKEN` | This allows Docker to query the Plex container for health checks.<br/><br/>See the [Plex documentation](https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/) for details on how to find this. | *none* |
| `TZ` (optional) | Time zone setting.<br/><br/><br/>**NOTE** this will affect scheduled jobs so it may be convenient to replace this with the appropriate value for your local time zone from the [list of tz database time zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones). | `Etc/UTC` |



### Plex
Before moving forward with other functionality, we need to set up a new Plex server and do some initial configuration:

* Start Plex by running `docker compose up plex`. After a few seconds, the output should indicate successful startup.
* Visit the web interface in your browser at http://localhost:32400/web.
* Choose a name for your server.
* Define a Media Library.
	* Set the library type to "TV Shows" and name the library "Formula 1". You'll be referencing this name later in the PMM configuration. You can choose a different name, but if you do you'll need to update it in the PMM config also.
	* Set the folder location to `/formula1`. This example project uses this path as its default location.
	* Under the Advanced settings, be sure to  **change the agent to "Personal Media Shows"**.
* Finish setup

### Plex Meta Manager
#### Basic setup
In the `config/plex-meta-manager/` directory, copy `config-example.yml` to `config.yml` and edit that file accordingly.
The [PMM Walkthrough](https://metamanager.wiki/en/latest/home/guides/local.html#setting-up-the-initial-config-file) contains generic instructions to follow, adapt as required for your setup.
The example configuration provided here removes extra functionality we won't be demonstrating.
First specify the minimum settings necessary for PMM to run:

1. TMDb API key
2. Plex URL and Token

#### F1-specific functionality
The PMM Wiki contains a nice [guide](https://metamanager.wiki/en/latest/home/guides/formula.html) for this, but generally you need to edit the `config/plex-media-manager/formula1.yml` metadata file to indicate which seasons you'd like to update metadata for:

```yml
metadata:
  2023:
    alt_title: "2023"
    f1_season: 2023
    round_prefix: true
```

Add additional entries for every season you'd like PMM to manage metadata for.

#### Run schedule
PMM runs at 5AM local time by default, so your F1 races will now show enhanced metadata until then.
If you'd like to deviate from that schedule, see the [documentation](https://metamanager.wiki/en/latest/metadata/details/schedule.html) for configuration options.

To trigger an initial one-time update for the first day, run the provided [pmm_initial_population.sh](pmm_initial_population.sh) script.

### Reddit F1 Scanner
This service will search Reddit for new race uploads on a set schedule depending on the day of the week (once a day during the week, more often during weekends when race activity is more likely).
To modify that schedule, see the documentation and add the appropriate environment variables to `docker-compose.yml`.

To trigger a one-time update, run the provided `reddit_f1_scanner_run.sh` script.

## Running
To start up the entire system, simply run `docker compose up --detach`.

Logs can be viewed with `docker compose logs`, or for individual services with `docker compose logs <container name>`.

Service URLs:
* Plex: http://localhost:32400/web
* Transmission: http://localhost:9091/transmission/web/
