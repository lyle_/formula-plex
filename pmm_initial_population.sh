#!/usr/bin/env bash

docker run \
	--rm \
	-it \
    -v "$(pwd)/data/plex-meta-manager:/config" \
    -v "$(pwd)/data/plex-meta-manager/assets:/config/assets" \
    -v "$(pwd)/config/plex-meta-manager/config.yml:/config/config.yml" \
    -v "$(pwd)/config/plex-meta-manager/formula1.yml:/config/formula1.yml" \
    --network formula-plex \
	meisnate12/plex-meta-manager --run

