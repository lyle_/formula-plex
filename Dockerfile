FROM lyleh/plex-f1-renamer:latest as build

FROM haugene/transmission-openvpn:dev
COPY --from=build /app/plex-f1-renamer /app/plex-f1-renamer
