#!/usr/bin/env bash
TRANSMISSION_HOST=transmission:9091

docker run \
	--rm \
	-it \
	--env TRANSMISSION_HOST=${TRANSMISSION_HOST} \
	--network formula-plex \
	lyleh/reddit-f1-scanner -oneshot
